
CREATE DATABASE IF NOT EXISTS metricsDB;
CREATE USER 'metrics'@'%' IDENTIFIED BY 'metrics';
GRANT CREATE, ALTER, INDEX, LOCK TABLES, REFERENCES, UPDATE, DELETE, DROP, SELECT , INSERT ON `metricsDB`.* TO 'metrics'@'%';
FLUSH PRIVILEGES;

USE metricsDB;

CREATE TABLE `repos` (
  `Id` int DEFAULT NULL,
  `RepoName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;




CREATE TABLE `pull_request` (
  `Repo` varchar(255) DEFAULT NULL,
  `AuthorName` varchar(255) DEFAULT NULL,
  `Id` int DEFAULT NULL,
  `Repo_Id` int DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `CreatedAt` datetime(3) DEFAULT NULL,
  `MergeUser` varchar(255) DEFAULT NULL,
  `MergedAt` datetime(3) DEFAULT NULL,
  `ClosedAt` datetime(3) DEFAULT NULL,
  `TargetBranch` varchar(255) DEFAULT NULL,
  `SourceBranch` varchar(255) DEFAULT NULL,
  `Sha` varchar(255) DEFAULT NULL,
  `MergedSha` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

