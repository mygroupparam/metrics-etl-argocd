# Metrics ETL

To run: go run main.go

kubectl get secrets
NAME            TYPE     DATA   AGE
mysql-secrets   Opaque   2      4h44m


kubectl describe secrets mysql-secrets
Name:         mysql-secrets
Namespace:    default
Labels:       app.kubernetes.io/instance=metricsdb
Annotations:  <none>

Type:  Opaque

Data
====
password:  4 bytes
username:  4 bytes

kubectl get configMap
NAME                  DATA   AGE
db-config             2      4h44m
istio-ca-root-cert    1      5d14h
kube-root-ca.crt      1      10d
mysql-initdb-config   1      4h44m




$  kubectl api-resources -o wide | grep secrets
secrets                                              v1                                     true         Secret                           create,delete,deletecollection,get,list,patch,update,watch
https://stackoverflow.com/questions/64551602/forbidden-systemserviceaccountdefaultdefault-cannot-create-resource-how-t
