package JsonResponseTypes

type ResponseTagList struct {
}

type ResponseTopicsList struct {
}

type ResponseSharedWithGroupsList struct {
}

type ComplianceFrameworksList struct {
}

type ResponseNamespaceStruct struct {
	Id        int    `json: "id"`
	Name      string `json: "name"`
	Path      string `json: "path"`
	Kinda     string `json: "kind"`
	FullPath  string `json: "full_path"`
	ParentId  int    `json: "parent_id"`
	AvatarUrl string `json: "avatar_url"`
	WebUrl    string `json: "web_url"`
}

type ResponseLinksStruct struct {
	Self          string `json: "self"`
	Issues        string `json: "issues"`
	MergeRequests string `json: "merge_requests"`
	RepoBranches  string `json: "repo_branches"`
	Labels        string `json: "labels"`
	Events        string `json: "events"`
	Memebers      string `json: "members"`
	ClusterAgents string `json: "cluster_agent"`
}

type ResponseContainerExpirationPolicyStruct struct {
	Cadence       string `json: "cadance"`
	Enabled       bool   `json: "enabled"`
	KeepN         int    `json: "keep_n"`
	OlderThan     string `json: "older_than"`
	NameRegex     string `json: "name_regex"`
	NameRegexKeep string `json: "name_regex_keep"`
	NextRunAt     string `json: "next_run_at"`
}

type ProjectAccessObject struct {
	AccessLevel       int `json:"access_level"`
	NotificationLevel int `json:"notification_level"`
}

type PermissionsObject struct {
	ProjectAccessObject
	GroupAccess string `json:"group_access"`
}

type JsonResponse struct {
	Id                                        int    `json: "id"`
	Description                               string `json: "description"`
	Name                                      string `json: "name"`
	NameWithNamespace                         string `json: "name_with_namespace"`
	Path                                      string `json: "path"`
	PathWithNamespace                         string `json: "path_with_namespace"`
	CreatedAt                                 string `json: "created_at"`
	DefaultBranch                             string `json: "default_branch"`
	TagList                                   ResponseTagList
	Topics                                    ResponseTopicsList
	SshUrlToRepo                              string `json: "ssh_url_to_repo"`
	HttpUrlToRepo                             string `json: "http_url_to_repo"`
	WebUrl                                    string `json: "web_url"`
	ReadmeUrl                                 string `json: "readme_url"`
	AvatarUrl                                 string `json: "avatar_url"`
	ForksCount                                int    `json: "forks_count"`
	StarCount                                 int    `json: "star_count"`
	LastActivity                              string `json: "last_activity_at"`
	Namespace                                 ResponseNamespaceStruct
	ContainerRegistryImagePrefix              string `json: "container_registry_image_prefix"`
	Links                                     ResponseLinksStruct
	PackageEnabled                            bool   `json: "packages_enabled"`
	EmptyRepo                                 bool   `json: "empty_repo"`
	Archived                                  bool   `json: "archived"`
	Visibility                                string `json: "visibility"`
	ResolveOutdatedDiff                       bool   `json: "resolve_outdated_diff_discussions"`
	ContainerExpirationPolicy                 ResponseContainerExpirationPolicyStruct
	IssuesEnabled                             bool   `json: "issues_enabled"`
	MergeRequestsEnabled                      bool   `json: "merge_requests_enabled"`
	WikiEnabled                               bool   `json: "wiki_enabled"`
	JobsEnabled                               bool   `json: "jobs_enabled"`
	SnippetsEnabled                           bool   `json: "snippets_enabled"`
	ContainerRegistryEnabled                  bool   `json: "container_registry_enabled"`
	ServiceDeskEnabled                        bool   `json: "service_desk_enabled"`
	ServiceDeskAddress                        string `json: "service_desk_address"`
	CanCreateMergeRequest                     bool   `json: "can_create_merge_request_in"`
	IssuesAccessLevel                         string `json: "issues_access_level"`
	RepositoryAccessLevel                     string `json: "repository_access_level"`
	MergeRequestsAccessLevel                  string `json: "merge_requests_access_level"`
	ForkingAccessLevel                        string `json: "forking_access_level"`
	WikiAccessLevel                           string `json: "wiki_access_level"`
	BuildsAccessLevel                         string `json: "builds_access_level"`
	SnippetsAccessLevel                       string `json: "snippets_access_level"`
	PagesAccessLevel                          string `json: "page_access_level"`
	OperationsAccessLevel                     string `json: "operations_access_level"`
	AnalyticsAccessLevel                      string `json: "analytics_access_level"`
	ContainerRegistryAccessLevel              string `json: "container_registry_access_level"`
	SecurityAndComplianceAccessLevel          string `json: "securyity_and_compliance_access_level"`
	EmailsDisabled                            string `json: "emails_disabled"`
	SharedRunnersEnabled                      bool   `json: "shared_runners_enabled"`
	LfsEnabled                                bool   `json: "lfs_enabled"`
	CreatorId                                 int    `json: "creator_id"`
	ImportStatus                              string `json: "import_status"`
	OpenIssuesCount                           int    `json: "open_issues_count"`
	CiDefaultGitDepth                         int    `json: "ci_default_git_depth"`
	CiForwardDeploymentEnabled                bool   `json: "ci_forward_deploytment_enabled"`
	CiJobTokenScopeEnabled                    bool   `json: "ci_job_token_scope_enabled"`
	CiSeparatedCaches                         bool   `json: "ci_separated_caches"`
	PublicJobs                                bool   `json: "public_jobs"`
	BuildTimeout                              int    `json: "build_timeout"`
	AutoCancelPendingPipelines                string `json: "auto_cancel_pending_pipelines"`
	BuildCoverageRegex                        string `json: "build_coverage_regex"`
	CiConfigPath                              string `json: "ci_config_path"`
	SharedWithGroups                          ResponseSharedWithGroupsList
	OnlyAllowMergeIfPipelineSucceeds          bool   `json: "only_allow_merge_if_pipline_succeeds"`
	AllowMergeOnSkippedPipeline               bool   `json: "allow_merge_on_skipped_pipeline"`
	RestrictUserDefinedVariables              bool   `json: "restrict_user_defined_variables"`
	RequestAccessEnabled                      bool   `json: "request_access_enabled"`
	OnlyAllowMergeIfAllDiscussionsAreResolved bool   `json: "only_allow_merge_if_all_discussions_are_resolved"`
	RemoveSourceBranchAfterMerge              bool   `json: "remove_source_branch_after_merge"`
	PrintingMergeReuqestLinkEnabled           bool   `json: "printing_merge_request_link_enabled"`
	MergeMethod                               string `json: "merge_method"`
	SquashOption                              string `json: "squash_option"`
	EnforceAuthChecksOnUploads                bool   `json: "enforce_auth_checks_on_uploads"`
	SuggesttionCommitMessage                  string `json: "suggestion_commit_message"`
	MergeCommitTemplate                       string `json: "merge_commit_template"`
	SquashCommitTemplate                      string `json: "squash_commit_template"`
	AutoDevopsEnabled                         bool   `json: "auto_devops_enabled"`
	AutoDevopsDeployStrategy                  bool   `json: "auto_devops_deploy_strategy"`
	KeepLatestArtifact                        bool   `json: "keep_latest_artifact"`
	RunnerTokenExpirationIntreval             string `json: "runner_token_expiration_interval"`
	ApprovalsBeforeMerge                      int    `json: "approvals_before_merge"`
	Mirror                                    bool   `json: "mirror"`
	ExternalAuthorizationClassificationLabel  string `json: "external_authorization_classification_label"`
	MarkedForDeletionAt                       string `json: "marked_for_deletion_at"`
	MarkedForDeletionOn                       string `json: "marked_for_deletion_on"`
	RequirementsEnabled                       bool   `json: "requirements_enabled"`
	RequirementsAccessLevel                   string `json: "requirements_access_level"`
	SecurityAndComplianceEnabled              bool   `json: "security_and_compliance_enabled"`
	ComplianceFrameworks                      ComplianceFrameworksList
	IssuesTemplate                            string `json: "issues_template"`
	MergeRequestsTemplate                     string `json: "merge_requests_template"`
	MergePipelinesEnabled                     bool   `json: "merge_pipelines_enabled"`
	MergeTrainsEnabled                        bool   `json: "merge_trains_enabled"`
	Permissions                               PermissionsObject
}
