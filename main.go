package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"metrics-etl/util"
	"net/http"
	"reflect"
	"strconv"
	"strings"

	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type PipelineHealthMerges struct {
	Author struct {
		Id         int    `json:"id"`
		Username   string `json:"username"`
		Name       string `json:"name"`
		State      string `json:"state"`
		Avatar_url string `json:"avatar"`
	} `json:"author"`
	Repo         string `json:"repo"`
	Id           int    `json:"id"`
	Title        string `json:"title"`
	Description  string `json:"description"`
	State        string `json:"state"`
	CreatedAt    string `json:"created_at"`
	MergeUser    string `json:"username"`
	MergedAt     string `json:"merged_at"`
	ClosedAt     string `json:"closed_at"`
	TargetBranch string `json:"target_branch"`
	SourceBranch string `json:"source_branch"`
	Sha          string `json:"sha"`
	MergedSha    string `json:"merge_commit_sha"`
}

type ReposStruct struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

// pagination
type Pagination struct {
	EndCursor   string `json:"endCursor"`
	HasNextPage bool   `json:"hasNextPage"`
}
type Response struct {
	Paginations Pagination `json:"pagination"`
}

type AllRepos []ReposStruct

// Global variables
var db *sql.DB
var repo_struct []ReposStruct
var pipelineHealthMerges_struct []PipelineHealthMerges
var allRepos AllRepos
var accessKeys []uint8
var config util.Config
var dbcredentials map[string]string

func init() {
	fmt.Println("Init in process")
	config, _ = util.LoadConfig()
	accessKeys = util.GetKVMap()
	dbcredentials = util.GetDBCredentialsformKubernetes()
	db = connectToDB()
	cleanup()
	fmt.Println("Init ended")
}

func cleanup() {
	// Initialize tables to insert fresh pull of data
	for _, table := range []string{"repos", "pull_request"} {
		_, err := db.Exec("DELETE  FROM " + table)
		if err != nil {
			panic(err.Error())
		}
	}
	allRepos = AllRepos{}
	fmt.Println("Tables initialized")
}

func connectToDB() *sql.DB {
	/*
		cfg := mysql.Config{
			User:                 config.Database.User,
			Passwd:               config.Database.Password,
			Net:                  "tcp",
			Addr:                 config.Database.Address + ":" + config.Database.Port,
			DBName:               config.Database.Name,
			AllowNativePasswords: true,
		}
	*/
	cfg := mysql.Config{
		User:                 dbcredentials["username"],
		Passwd:               dbcredentials["password"],
		Net:                  "tcp",
		Addr:                 dbcredentials["host"] + ":" + dbcredentials["port"],
		DBName:               dbcredentials["dbName"],
		AllowNativePasswords: true,
	}
	// Get a database handle.
	fmt.Println("cfg.FormatDSN()", cfg.FormatDSN())
	var err error
	db, err = sql.Open("mysql", cfg.FormatDSN())
	if err != nil {
		fmt.Println("mysql  Error", err)
	}
	pingErr := db.Ping()
	if pingErr != nil {
		fmt.Println("DB Ping  Error", pingErr)
		log.Fatal(pingErr)
	}
	fmt.Println("Connected!")
	return db
}

func getRequestObject(url string, accessToken string) *http.Request {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Add("Authorization", accessToken)
	return req
}

func getTypeName(t reflect.Type) string {
	return t.Name()
}

func doPagination(task string, repo string, req *http.Request) ([]Response, error) {
	//	fmt.Println("Process: ", task)
	//	fmt.Println("Repo: ", repo)
	var responses []Response
	var response Response
	// Get our initial response from the API and capture status code
	resp, _ := http.DefaultClient.Do(req)
	// Read the response body and Unmarshal into struct
	respBody, err := ioutil.ReadAll(resp.Body)
	json.Unmarshal(respBody, &response)

	// If there was a parsing error, log it
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	// Append response to our slice of responses
	responses = append(responses, response)

	// If paginations are available, continue to loop through until all paginations are exhausted
	var loopTotal int
	totalRecordcount, _ := strconv.Atoi(resp.Header.Get("x-total"))
	perpagerecords, _ := strconv.Atoi(resp.Header.Get("x-per-page"))
	//	fmt.Println("totalRecordcount:", totalRecordcount)
	//	fmt.Println("perpagerecords:", perpagerecords)
	//Check whether or not  pull data from GitLab is empty
	if totalRecordcount > 0 && perpagerecords > 0 {
		loopTotal = totalRecordcount / perpagerecords
	} else {
		loopTotal = 0
	}

	loops := loopTotal + 1

	count := 1
	if loops > 0 {
		response.Paginations.HasNextPage = true
		response.Paginations.EndCursor = "1"
	}
	for response.Paginations.HasNextPage == true {
		// Append "after" cursor to query in order to redirect to paginated response
		qry := req.URL.Query()
		qry.Set("page", response.Paginations.EndCursor)

		req.URL.RawQuery = qry.Encode()

		// Make request
		resp, err := http.DefaultClient.Do(req)
		// Read response and deserialize it
		respBody, err := ioutil.ReadAll(resp.Body)
		json.Unmarshal(respBody, &response)
		if task == "populateRepoTable" {
			//		cleanup()
			json.Unmarshal(respBody, &repo_struct)
			insertRepos(repo_struct)
		} else {
			json.Unmarshal(respBody, &pipelineHealthMerges_struct)
			//	fmt.Println("pipelineHealthMerges_struct", pipelineHealthMerges_struct)
			//	fmt.Println("pipelineHealthMerges_struct", len(pipelineHealthMerges_struct))

			if len(pipelineHealthMerges_struct) > 0 {
				construct_PipelineHealthMetrics_Merges(repo, pipelineHealthMerges_struct)
			} else {
				fmt.Println("repo being rejected due to empty pipelineHealthMerges_struct: ", repo)
			}
		}

		// If there was a parsing error, log it
		if err != nil {
			fmt.Println(err)
		}
		defer resp.Body.Close()
		// Append response to our slice of responses
		responses = append(responses, response)
		loops = loops - 1
		count = count + 1
		response.Paginations.EndCursor = strconv.Itoa(count)
		if loops == 0 {
			response.Paginations.HasNextPage = false
		}
	}
	return responses, nil
}

func prepareDbTables() {

	if len(accessKeys) == 0 {
		fmt.Println("No records inserted into pull_request as key value json file is empty!")
	}

	// Make API call to get repos from Gitlanb and insert records in db. Ensure Pagination
	token := "Bearer " + config.AccessKeys.Repos
	req := getRequestObject(config.GitlabUrl.Repo, token)
	doPagination("populateRepoTable", "", req)

	// Make API call to get merge requests from Gitlanb and insert into db
	for _, values := range allRepos {
		token := "Bearer " + config.AccessKeys.Repos
		url := config.GitlabUrl.Merges + "/" + strconv.Itoa(values.Id) + "/merge_requests?per_page=100"
		//	fmt.Println("URL: ", url)
		req = getRequestObject(url, token)
		doPagination("merges", values.Name, req)
	}
}

func construct_PipelineHealthMetrics_Merges(repo string, data []PipelineHealthMerges) {

	for _, values := range data {

		var args []interface{}
		args = append(args, repo)
		args = append(args, values.Id)
		args = append(args, values.Author.Name)
		args = append(args, values.Title)
		args = append(args, values.State)
		args = append(args, values.MergeUser)
		args = append(args, values.SourceBranch)
		args = append(args, values.Sha)
		args = append(args, values.MergedSha)
		args = append(args, values.TargetBranch)

		query1 := "INSERT INTO `metricsDB`.`pull_request` (`Repo`,`Id`, `AuthorName`,`Title`,`State`,`MergeUser`,`SourceBranch`,`Sha`,`MergedSha`,`TargetBranch` "
		query2 := " VALUES (?,?,?,?,?,?,?,?,?,?"

		createdAt := values.CreatedAt
		createdAt = strings.Replace(createdAt, "T", " ", -1)
		createdAt = strings.Replace(createdAt, "Z", "", -1)
		args = append(args, createdAt)
		query1 = query1 + ",`CreatedAt`"
		query2 = query2 + ",?"
		mergedAt := values.MergedAt
		closedAt := values.ClosedAt
		if mergedAt == "null" {
			mergedAt = ""
		}
		if closedAt == "null" {
			closedAt = ""
		}

		if len(mergedAt) > 0 {
			mergedAt = strings.Replace(mergedAt, "T", " ", -1)
			mergedAt = strings.Replace(mergedAt, "Z", "", -1)
			query1 = query1 + ",`MergedAt`"
			query2 = query2 + ",?"
			args = append(args, mergedAt)
		}

		if len(closedAt) > 0 {
			closedAt = strings.Replace(closedAt, "T", " ", -1)
			closedAt = strings.Replace(closedAt, "Z", "", -1)
			query1 = query1 + ",`ClosedAt`"
			query2 = query2 + ",?"
			args = append(args, closedAt)
		}
		query1 = query1 + ")"
		query2 = query2 + ")"
		query := query1 + query2

		insert, _ := db.Prepare(query)
		_, err := insert.Exec(args...)
		insert.Close()
		if err != nil {
			panic(err.Error())
		}
		defer insert.Close()
	}
	//os.Exit(3)
}

func insertRepos(data []ReposStruct) {
	fmt.Println("Preparing Repos Table")
	for _, values := range data {
		allRepos = append(allRepos, values)
		query := "INSERT INTO `metricsDB`.`repos` (`Id`, `RepoName`)  VALUES (?,?)"
		insert, _ := db.Prepare(query)
		_, err := insert.Exec(values.Id, values.Name)
		insert.Close()
		if err != nil {
			panic(err.Error())
		}
		defer insert.Close()
	}
	fmt.Println("Records inserted into  Repos Table")
	//os.Exit(3)
}

func main() {
	go cleanup()
	prepareDbTables()
	fmt.Println("Process completed! May proceed to see dashboard")
	fmt.Println("Process Ends!")

	/*
		secs_str := config.Scrapping.Interval
		secs_int, _ := strconv.Atoi(secs_str)

		var timesecs int
		timesecs = secs_int

		go func() {
			for true {
				fmt.Println("Process kicked off at:", time.Now().String())
				go cleanup()
				prepareDbTables()
				fmt.Println("Process completed! May proceed to see dashboard")
				fmt.Println("Exporter does GitLab scrapping in  next " + config.Scrapping.Interval + "  seconds.")
				time.Sleep(time.Duration(timesecs) * time.Second)

			}
		}()
	*/
	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":9091", nil)

}
