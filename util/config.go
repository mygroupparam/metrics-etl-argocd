package util

import (
	"fmt"
	"io/ioutil"
	"os"

	"context"

	"github.com/spf13/viper"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

type Config struct {
	Database struct {
		Address  string `mapstructure:"address"`
		Port     string `mapstructure:"port"`
		User     string `mapstructure:"user"`
		Password string `mapstructure:"password"`
		Name     string `mapstructure:"name"`
	} `json:"database"`
	AccessKeys struct {
		Repos string `mapstructure:"repos"`
	} `json:"accessKeys"`
	Scrapping struct {
		Interval string `mapstructure:"interval"`
	} `json:"scrapping"`
	GitlabUrl struct {
		Repo   string `mapstructure:"repo"`
		Merges string `mapstructure:"merges"`
	} `json:"gitlabUrl"`
}

var vp *viper.Viper

func GetKVMap() []uint8 {
	configFile, _ := os.Open("util/key_values.json")
	keyValues, _ := ioutil.ReadAll(configFile)
	if len(keyValues) == 0 {
		fmt.Println("Error: Empty JSON input")
		return nil
	}
	fmt.Println("Grabbed key values")
	return keyValues

}

func LoadConfig() (Config, error) {
	vp = viper.New()
	var config Config
	vp.SetConfigName("config")
	vp.SetConfigType("json")
	vp.AddConfigPath("./util")
	vp.AddConfigPath(".")

	err := vp.ReadInConfig()

	if err != nil {
		return Config{}, err
	}
	err = vp.Unmarshal(&config)

	if err != nil {
		return Config{}, err
	}
	fmt.Println("Config file loaded successfully!")
	return config, nil
}
func GetDBCredentialsformKubernetes() map[string]string {
	db_credentials := make(map[string]string)
	rules := clientcmd.NewDefaultClientConfigLoadingRules()
	kubeconfig := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(rules, &clientcmd.ConfigOverrides{})
	config, err := kubeconfig.ClientConfig()
	if err != nil {
		panic(err)
	}
	clientset := kubernetes.NewForConfigOrDie(config)
	retrievedSecret, err := clientset.CoreV1().Secrets("default").Get(context.Background(), "mysql-secrets", metav1.GetOptions{})
	if err != nil {
		panic(err)
	}
	// retrieve the secret data
	username := string(retrievedSecret.Data["username"])
	password := string(retrievedSecret.Data["password"])
	db_credentials["username"] = username
	db_credentials["password"] = password

	retrievedconfig, err := clientset.CoreV1().ConfigMaps("default").Get(context.Background(), "db-config", metav1.GetOptions{})
	if err != nil {
		panic(err)
	}
	// retrieve the secret data
	dbName := string(retrievedconfig.Data["dbName"])
	host := string(retrievedconfig.Data["host"])
	port := string(retrievedconfig.Data["port"])

	fmt.Println("port:", port)
	db_credentials["host"] = host
	db_credentials["dbName"] = dbName
	db_credentials["port"] = port
	fmt.Println("db_credentials:", db_credentials)

	return db_credentials
}
